# Math-SIBs example app

This example illustrates how to create a SIB that references a `Math` model, which is a new type of graph model that is not yet available in DIME.

At generation time, from such a Math-SIB executable Java code is generated to be executed from within the code that is generated for the containing Process model.

## Setup the plugin project and run the product

Head over to the [generic-math-sibs](https://gitlab.com/scce/dime-example-extensions/generic-math-sibs) extension project and follow the instructions there to setup the workspace project that is needed to add Math-SIBs support to DIME.

Run the new instance of DIME including the Math-SIBs (step 'Run the product' in the other documentation) before proceeding with the next step of this documentation.

## Setup the runtime project

Clone this repository to a folder that will be referred to as 'repo location' in the following.

From the context menu of the Project Explorer select 'Import > Existing projects into workspace'.
In the opening dialog, enter your repo location as root directory.
Mark the checkbox of the `GenericMathSIBApp` project and hit 'Finish'.

Have a look at the demo model `dime-models/interaction/Home.process` that uses the new Math-SIBs.

Feel free to try out adding other Math-SIBs on your own by dragging and dropping the `.math` files containing MathGraph models in folder `mathgraphs` to a Process model and selecting to create a GenericSIB.

To build and deploy the app, hit the 'Purge and Deploy' button in the Deployment View to build and run the app.
As soon as the app has been deployed you can access it by opening http://127.0.0.1:9090 in a browser.